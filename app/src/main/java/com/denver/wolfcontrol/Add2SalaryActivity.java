package com.denver.wolfcontrol;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.denver.wolfcontrol.data.SalaryContract;
import com.denver.wolfcontrol.data.SalaryDbHelper;

import java.util.Collections;
import java.util.List;

public class Add2SalaryActivity extends AppCompatActivity {

    private Spinner monthSpinner;
    private int day, month, year;
    private int setMonth;
    private int setYear;

    private EditText mEditTextSumm;
    private EditText mEditTextNotes;
    private EditText mEditTextYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add2_salary);

        mEditTextSumm = (findViewById(R.id.editTextSumm));
        mEditTextNotes = (findViewById(R.id.editTextNotes));
        mEditTextYear = (findViewById(R.id.editTextYear));


        //receive DatePicker`s date from intent objects
        Intent intent = getIntent();
        day = intent.getIntExtra(MainActivity.EXTRA_DAY, 0);
        month = intent.getIntExtra(MainActivity.EXTRA_MONTH, 0);
        year = intent.getIntExtra(MainActivity.EXTRA_YEAR, 0);

        TextView header = findViewById(R.id.add2SalaryHeader);
        header.setText("Выдано\n" + day + " / " + month + " / " + year);
        EditText yearText = findViewById(R.id.editTextYear);
        yearText.setText(String.valueOf(year));

        monthSpinner = findViewById(R.id.spinner_month);
        setupSpinner();


        SQLiteDatabase db = getBaseContext().openOrCreateDatabase("salary.db", MODE_PRIVATE, null);

        Cursor query = db.rawQuery("SELECT * FROM money;", null);
            if(query.moveToFirst()) {
                do {
                    String rawDate = query.getString(1);
                    int rawSumm = query.getInt(2);
                    int rawMonth = query.getInt(3);
                    int rawYear = query.getInt(4);
                    String rawNotes = query.getString(5);
                    if (rawDate.equals(day + "-" + month + "-" + year)) {
                        mEditTextSumm.setText(String.valueOf(rawSumm));
                        mEditTextYear.setText(String.valueOf(rawYear));
                        mEditTextNotes.setText(rawNotes);
                        monthSpinner.setSelection(rawMonth-1);
                    }
                } while (query.moveToNext());
            }
            query.close();
            db.close();

    }

    private void setupSpinner() {
      ArrayAdapter monthSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.array_month, android.R.layout.simple_spinner_item);
      monthSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

      monthSpinner.setAdapter(monthSpinnerAdapter);
      monthSpinner.setSelection(0);

      monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
     @Override
     public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selection = (String) parent.getItemAtPosition(position);
            if(!TextUtils.isEmpty(selection)) {
                if(selection.equals(getString(R.string.jan))) {
                    setMonth = SalaryContract.MoneyEntry.MONTH_JAN;
                } else if(selection.equals(getString(R.string.fab))) {
                    setMonth = SalaryContract.MoneyEntry.MONTH_FAB;
                } else if(selection.equals(getString(R.string.mar))) {
                    setMonth = SalaryContract.MoneyEntry.MONTH_MAR;
                } else if(selection.equals(getString(R.string.apr))) {
                    setMonth = SalaryContract.MoneyEntry.MONTH_APR;
                } else if(selection.equals(getString(R.string.may))) {
                    setMonth = SalaryContract.MoneyEntry.MONTH_MAY;
                } else if(selection.equals(getString(R.string.jun))) {
                    setMonth = SalaryContract.MoneyEntry.MONTH_JUN;
                } else if(selection.equals(getString(R.string.jul))) {
                    setMonth = SalaryContract.MoneyEntry.MONTH_JUL;
                } else if(selection.equals(getString(R.string.aug))) {
                    setMonth = SalaryContract.MoneyEntry.MONTH_AUG;
                } else if(selection.equals(getString(R.string.sep))) {
                    setMonth = SalaryContract.MoneyEntry.MONTH_SEP;
                } else if(selection.equals(getString(R.string.oct))) {
                    setMonth = SalaryContract.MoneyEntry.MONTH_OCT;
                } else if(selection.equals(getString(R.string.nov))) {
                    setMonth = SalaryContract.MoneyEntry.MONTH_NOV;
                } else if(selection.equals(getString(R.string.dec))) {
                    setMonth = SalaryContract.MoneyEntry.MONTH_DEC;
                }
            }
     }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            setMonth = 1;
     }
      });

    }

    //Сохраняем введенные данные в базе данных.
    public void insertMoney (View view) {

        //Проверка на пустое поле в Сумме
        String checkSumm = mEditTextSumm.getText().toString().trim();
        if(checkSumm.equals("")) {
            Toast.makeText(this, "Пустое значение поля СУММА ! ", Toast.LENGTH_LONG).show();
            return;
        }

        String date = day + "-" + month + "-" + year;

        // Удаляем старую строку с этой датой
        SQLiteDatabase db = getBaseContext().openOrCreateDatabase("salary.db", MODE_PRIVATE, null);
        String SQL_DELETE_OLD_RECORD = "DELETE FROM money WHERE date = '" + date +"';";
        db.execSQL(SQL_DELETE_OLD_RECORD);
        db.close();

    int summ = Integer.parseInt(mEditTextSumm.getText().toString().trim());
    String notes = mEditTextNotes.getText().toString().trim();
    String setYear = mEditTextYear.getText().toString().trim();

        SalaryDbHelper mDbHelper = new SalaryDbHelper(this);

        db = mDbHelper.getWritableDatabase();



        ContentValues values = new ContentValues();
        values.put(SalaryContract.MoneyEntry.COLUMN_DATE, date);
        values.put(SalaryContract.MoneyEntry.COLUMN_SUMM, summ);
        values.put(SalaryContract.MoneyEntry.COLUMN_MONTH, setMonth);
        values.put(SalaryContract.MoneyEntry.COLUMN_YEAR, setYear);
        values.put(SalaryContract.MoneyEntry.COLUMN_NOTE, notes);

        // Вставляем новый ряд в базу данных и запоминаем его идентификатор
        long newRowId = db.insert(SalaryContract.MoneyEntry.TABLE_NAME, null, values);

        // Выводим сообщение в успешном случае или при ошибке
        if (newRowId == -1) {
            // Если ID  -1, значит произошла ошибка
            Toast.makeText(this, "Ошибка при записи!", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Запись успешно заведена под номером: " + newRowId, Toast.LENGTH_LONG).show();
            finish();

        }


    }
    // Удаление строки из базы данных
    public void deleteMoney (View view) {
        String date = day + "-" + month + "-" + year;
        SQLiteDatabase db = getBaseContext().openOrCreateDatabase("salary.db", MODE_PRIVATE, null);
        String SQL_DELETE_RECORD = "DELETE FROM money WHERE date = '" + date +"';";
        db.execSQL(SQL_DELETE_RECORD);
        db.close();
        Toast.makeText(this, "Запись очищена! ", Toast.LENGTH_LONG).show();
        finish();
    }
}