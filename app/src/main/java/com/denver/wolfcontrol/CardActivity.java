package com.denver.wolfcontrol;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CardActivity extends AppCompatActivity {
    public String fileName; // Reserve variable for File Name of the selected card
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        EditText moneyEditNumber1 = findViewById(R.id.moneyEditNumber1);
        EditText moneyEditNumber2 = findViewById(R.id.moneyEditNumber2);
        EditText moneyEditNumber3 = findViewById(R.id.moneyEditNumber3);
        EditText moneyEditNumber4 = findViewById(R.id.moneyEditNumber4);
        EditText hoursEditNumber1 = findViewById(R.id.hoursEditNumber1);
        EditText hoursEditNumber2 = findViewById(R.id.hoursEditNumber2);
        EditText hoursEditNumber3 = findViewById(R.id.hoursEditNumber3);
        EditText hoursEditNumber4 = findViewById(R.id.hoursEditNumber4);
        TextView summTextView = findViewById(R.id.summTextView2);
        Switch switchMain = findViewById(R.id.switchMain);
        Switch switchSubmain = findViewById(R.id.switchSubmain);


        //Pre-Set fields values for new card
        moneyEditNumber1.setText("0");
        moneyEditNumber2.setText("0");
        moneyEditNumber3.setText("0");
        moneyEditNumber4.setText("0");
        hoursEditNumber1.setText("");
        hoursEditNumber2.setText("");
        hoursEditNumber3.setText("");
        hoursEditNumber4.setText("");
        summTextView.setText("0");


        //receive DatePicker`s date from intent objects
        Intent intent = getIntent();
        int day = intent.getIntExtra(MainActivity.EXTRA_DAY, 0);
        int month = intent.getIntExtra(MainActivity.EXTRA_MONTH, 0);
        int year = intent.getIntExtra(MainActivity.EXTRA_YEAR, 0);


        // Rename number of the month in word
        String monthString;
        switch (month) {
                case 1:  monthString = "Января";
                    break;
                case 2:  monthString = "Февраля";
                    break;
                case 3:  monthString = "Марта";
                    break;
                case 4:  monthString = "Апреля";
                    break;
                case 5:  monthString = "Мая";
                    break;
                case 6:  monthString = "Июня";
                    break;
                case 7:  monthString = "Июля";
                    break;
                case 8:  monthString = "Августа";
                    break;
                case 9:  monthString = "Сентября";
                    break;
                case 10: monthString = "Октября";
                    break;
                case 11: monthString = "Ноября";
                    break;
                case 12: monthString = "Декабря";
                    break;
                default: monthString = "Не знаем такого";
                break;
        }

        // Make Date for dateTextView
        String date = day + " " + monthString + " " + year;

        TextView dateTextView = findViewById(R.id.dateTextView);
        dateTextView.setText(date);


         // Make fileName
         String dayFileName;
         if (day-10 < 0) {
             dayFileName = "0" + day;
         } else dayFileName = String.valueOf(day);
         String monthFileName;
         if (month-10 < 0) {
             monthFileName = "0" + month;
         } else monthFileName = String.valueOf(month);
         String yearFileName = String.valueOf(year);

        fileName = dayFileName + monthFileName + yearFileName + ".txt";

        //Try to read DBFile fileName.txt (*date*.txt)
        FileInputStream fin = null;
        try {
            fin = openFileInput(fileName);
            byte[] bytes = new byte[fin.available()];
            fin.read(bytes);
            String buffer = new String (bytes);
            fin.close();

            // Processing file information
            char[] chArray = buffer.toCharArray();
            int k=0;

            String hours1 = "";
            for (int i = 0; i<chArray.length; i++){
                k = i;
                if (chArray[i] != '\n') {
                    hours1 = hours1 + chArray[i];
                } else break;

            }
            String hours2 = "";
            for (int i = k+1; i<chArray.length; i++){
                k = i;
                if (chArray[i] != '\n') {
                    hours2 = hours2 + chArray[i];
                } else break;

            }
            String hours3 = "";
            for (int i = k+1; i<chArray.length; i++){
                k = i;
                if (chArray[i] != '\n') {
                    hours3 = hours3 + chArray[i];
                } else break;

            }
            String hours4 = "";
            for (int i = k+1; i<chArray.length; i++){
                k = i;
                if (chArray[i] != '\n') {
                    hours4 = hours4 + chArray[i];
                } else break;

            }
            String money1 = "";
            for (int i = k+1; i<chArray.length; i++){
                k = i;
                if (chArray[i] != '\n') {
                    money1 = money1 + chArray[i];
                } else break;

            }
            String money2 = "";
            for (int i = k+1; i<chArray.length; i++){
                k = i;
                if (chArray[i] != '\n') {
                    money2 = money2 + chArray[i];
                } else break;

            }
            String money3 = "";
            for (int i = k+1; i<chArray.length; i++){
                k = i;
                if (chArray[i] != '\n') {
                    money3 = money3 + chArray[i];
                } else break;

            }
            String money4 = "";
            for (int i = k+1; i<chArray.length; i++){
                k = i;
                if (chArray[i] != '\n') {
                    money4 = money4 + chArray[i];
                } else break;

            }
            String summ = "";
            for (int i = k+1; i<chArray.length; i++){
                k = i;
                if (chArray[i] != '\n') {
                    summ = summ + chArray[i];
                } else break;

            }
            // Get Switchers status 0 or 1 or 2
            char swStatus = '0';
            for (int i = k+1; i<chArray.length; i++){
                k = i;
                if (chArray[i] != '\n') {
                    swStatus = chArray[i];
                } else break;

            }
            // Set Edit and Text fields with the information from file
            hoursEditNumber1.setText(hours1);
            hoursEditNumber2.setText(hours2);
            hoursEditNumber3.setText(hours3);
            hoursEditNumber4.setText(hours4);
            moneyEditNumber1.setText(money1);
            moneyEditNumber2.setText(money2);
            moneyEditNumber3.setText(money3);
            moneyEditNumber4.setText(money4);
            summTextView.setText(summ);

            // Set switchers
            if (swStatus == '1') {
                switchMain.setChecked(true);
            }
            if (swStatus == '2') {
                switchSubmain.setChecked(true);
            }


        } catch (IOException ex) {
            Toast.makeText(this, "Новая карта", Toast.LENGTH_LONG).show(); // if we have no file
        }

        // Protect from switches checked TOGETHER
        switchMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Switch switchSubmain = findViewById(R.id.switchSubmain);
                switchSubmain.setChecked(false);
            }
        });

        switchSubmain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Switch switchMain = findViewById(R.id.switchMain);
                switchMain.setChecked(false);
            }
        });


    }

    // Processing saveButton
     public void save (View view) {

        String money1s;
        String money2s;
        String money3s;
        String money4s;
        String moneySummS;
        String hours1s;
        String hours2s;
        String hours3s;
        String hours4s;
        int money1;
        int money2;
        int money3;
        int money4;
        int moneySumm;

         EditText moneyEditNumber1 = findViewById(R.id.moneyEditNumber1);
         EditText moneyEditNumber2 = findViewById(R.id.moneyEditNumber2);
         EditText moneyEditNumber3 = findViewById(R.id.moneyEditNumber3);
         EditText moneyEditNumber4 = findViewById(R.id.moneyEditNumber4);
         EditText hoursEditNumber1 = findViewById(R.id.hoursEditNumber1);
         EditText hoursEditNumber2 = findViewById(R.id.hoursEditNumber2);
         EditText hoursEditNumber3 = findViewById(R.id.hoursEditNumber3);
         EditText hoursEditNumber4 = findViewById(R.id.hoursEditNumber4);

         hours1s = hoursEditNumber1.getText().toString();
         hours2s = hoursEditNumber2.getText().toString();
         hours3s = hoursEditNumber3.getText().toString();
         hours4s = hoursEditNumber4.getText().toString();

         money1s = moneyEditNumber1.getText().toString();
         if(money1s.equals("")) money1s = "0"; // check for empty fields, if true change to 0
         money1 = Integer.parseInt(money1s);

         money2s = moneyEditNumber2.getText().toString();
         if(money2s.equals("")) money2s = "0"; // check for empty fields, if true change to 0
         money2 = Integer.parseInt(money2s);

         money3s = moneyEditNumber3.getText().toString();
         if(money3s.equals("")) money3s = "0"; // check for empty fields, if true change to 0
         money3 = Integer.parseInt(money3s);

         money4s = moneyEditNumber4.getText().toString();
         if(money4s.equals("")) money4s = "0"; // check for empty fields, if true change to 0
         money4 = Integer.parseInt(money4s);

         moneySumm = money1 + money2 + money3 + money4;
         moneySummS = String.valueOf(moneySumm);

        TextView summTextView2 = findViewById(R.id.summTextView2);
        summTextView2.setText(moneySummS);

        //Switches processing
         Switch switchMain = findViewById(R.id.switchMain);
         Switch switchSubmain = findViewById(R.id.switchSubmain);
         String swStatus = "0";


         // Check if NO ONE switch is checked.
         if (switchMain.isChecked() || switchSubmain.isChecked()) {
                      } else {
             Toast.makeText(this, "Это СМЕНА или ПОДРАБОТКА ?", Toast.LENGTH_LONG).show();
             return;
             }
        // Make Switchers status for save
        if (switchMain.isChecked()) {
            swStatus = "1";
        }
        if (switchSubmain.isChecked()) {
            swStatus = "2";
        }

        // Make information for save in DBFile
         String text = hours1s + "\n" + hours2s + "\n" + hours3s + "\n" + hours4s + "\n" + money1s + "\n" + money2s + "\n" + money3s + "\n" + money4s + "\n" + moneySummS + "\n" + swStatus + "\n";

        //Try to write DBFile fileName.txt (*date*.txt)
         FileOutputStream fos = null;
         try {
             fos = openFileOutput(fileName, MODE_PRIVATE);
             fos.write(text.getBytes());
             Toast.makeText(this, "Карточка сохранена успешно !", Toast.LENGTH_LONG).show();
             fos.close();

         }catch (IOException ex){
             Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
         }

     }

        public void deleteCard (View view) {
            File file = new File(getFilesDir(),fileName);
            boolean control = file.delete();
            if(control) {
                Toast.makeText(this, "Карточка удалена !", Toast.LENGTH_LONG).show();
                finish();
            } else Toast.makeText(this, "Карточка и так новая! Что тут удалять?", Toast.LENGTH_LONG).show();


        }

     }

