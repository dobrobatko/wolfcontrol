package com.denver.wolfcontrol;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.TextView;

public class SalaryReportActivity extends AppCompatActivity {

    public int totalSumm = 0;
    public String firstReportBody;
    public String secondReportBody = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salary_report);

        Intent intent = getIntent();
        int month = intent.getIntExtra(StatActivity.REPORT_MONTH,0);
        int year = intent.getIntExtra(StatActivity.REPORT_YEAR,0);

        TextView reportBody = findViewById(R.id.salaryReportBody);

        // Rename number of the month in word
        String monthString;
        switch (month) {
            case 1:  monthString = "Январь";
                break;
            case 2:  monthString = "Февраль";
                break;
            case 3:  monthString = "Март";
                break;
            case 4:  monthString = "Апрель";
                break;
            case 5:  monthString = "Май";
                break;
            case 6:  monthString = "Июнь";
                break;
            case 7:  monthString = "Июль";
                break;
            case 8:  monthString = "Август";
                break;
            case 9:  monthString = "Сентябрь";
                break;
            case 10: monthString = "Октябрь";
                break;
            case 11: monthString = "Ноябрь";
                break;
            case 12: monthString = "Декабрь";
                break;
            default: monthString = "Не знаем такого";
                break;
        }

        TextView reportHeader = findViewById(R.id.reportHeader);
        reportHeader.setText("Зарплата за " + monthString + " " + year + " " + getString(R.string.reportHeader2));

        SQLiteDatabase db = getBaseContext().openOrCreateDatabase("salary.db", MODE_PRIVATE, null);

        Cursor query = db.rawQuery("SELECT * FROM money", null);
            if(query.moveToFirst()) {
                do {
                    String rawDate = query.getString(1);
                    int rawSumm = query.getInt(2);
                    int rawMonth = query.getInt(3);
                    int rawYear = query.getInt(4);
                    String rawNotes = query.getString(5);
                    if (rawMonth == month && rawYear == year) {
                        totalSumm = totalSumm + rawSumm;
                        if(rawNotes.equals("")) {
                            secondReportBody = secondReportBody + "" + rawDate + " выдано: " + rawSumm + "\n\n";
                            } else secondReportBody = secondReportBody + "" + rawDate + " выдано: " + rawSumm + "\nПримечание:\n" + rawNotes + "\n\n";
                    }

                }while (query.moveToNext());
            }
            query.close();
            db.close();

            firstReportBody = "Всего выплачено за " + monthString + " : " + totalSumm + "\n----------------------\n\n";
            reportBody.setText(firstReportBody + secondReportBody);

    }
}