package com.denver.wolfcontrol;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;

import com.denver.wolfcontrol.data.SalaryDbHelper;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_DAY = "EXTRA_DAY";
    public final static String EXTRA_MONTH = "EXTRA_MONTH";
    public final static String EXTRA_YEAR = "EXTRA_YEAR";

    private SalaryDbHelper mDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Deny change orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        DatePicker datePicker = findViewById(R.id.datePickerMain);
// get current date from Calendar
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

// set current date into datepicker
        datePicker.init(year, month, day, null);

        //Initializing new DB
        mDbHelper = new SalaryDbHelper(this);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        db.close();
    }

    // processing click on ChooseButton
    public void choose(View view) {
        DatePicker datePicker = findViewById(R.id.datePickerMain);
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth() + 1;
        int year = datePicker.getYear();


        // start Card Activity
        Intent intent = new Intent(this, CardActivity.class);
        intent.putExtra(EXTRA_DAY, day);
        intent.putExtra(EXTRA_MONTH, month);
        intent.putExtra(EXTRA_YEAR, year);
        startActivity(intent);


    }

    public void statistic(View view) {
        Intent intent = new Intent(this, StatActivity.class);
        startActivity(intent);
    }

    public void aboutInfo(View view) {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    public void salary(View view) {
        Intent intent = new Intent(this, SalaryActivity.class);
        startActivity(intent);
    }

    public void addSalary(View view) {
        DatePicker datePicker = findViewById(R.id.datePickerMain);
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth() + 1;
        int year = datePicker.getYear();


        // start Add2Salary Activity
        Intent intent = new Intent(this, Add2SalaryActivity.class);
        intent.putExtra(EXTRA_DAY, day);
        intent.putExtra(EXTRA_MONTH, month);
        intent.putExtra(EXTRA_YEAR, year);
        startActivity(intent);


    }
}