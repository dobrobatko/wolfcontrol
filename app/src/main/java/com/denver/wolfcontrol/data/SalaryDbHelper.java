package com.denver.wolfcontrol.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import androidx.appcompat.app.AppCompatActivity;

import com.denver.wolfcontrol.data.SalaryContract.MoneyEntry;

import java.io.File;
import java.io.FileOutputStream;

public class SalaryDbHelper extends SQLiteOpenHelper {

    public static final String LOG_TAG = SalaryDbHelper.class.getSimpleName();

    // Имя файла базы данных
    private static final String DATABASE_NAME = "salary.db";
    // Версия базы данных
    private static final int DATABASE_VERSION = 1;

    /**
     // Конструктор {@link SalaryDbHelper}.
     *
     * @param context Контекст приложения
     */
    public SalaryDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Вызывается при создании базы данных
    @Override
    public void onCreate(SQLiteDatabase db) {
        // Строка для создания таблицы
        String SQL_CREATE_MONEY_TABLE = "CREATE TABLE " + MoneyEntry.TABLE_NAME + " ("
            + MoneyEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + MoneyEntry.COLUMN_DATE + " TEXT NOT NULL, "
            + MoneyEntry.COLUMN_SUMM + " INTEGER NOT NULL DEFAULT 0, "
            + MoneyEntry.COLUMN_MONTH + " INTEGER NOT NULL DEFAULT 1, "
            + MoneyEntry.COLUMN_YEAR + " INTEGER NOT NULL DEFAULT 0, "
            + MoneyEntry.COLUMN_NOTE + " TEXT);";

        // Запускаем создание таблицы
        db.execSQL(SQL_CREATE_MONEY_TABLE);



    }

    // Вызывается при обновлении схемы базы даннных
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
