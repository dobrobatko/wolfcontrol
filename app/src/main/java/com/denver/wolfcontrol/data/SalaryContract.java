package com.denver.wolfcontrol.data;

import android.provider.BaseColumns;

public final class SalaryContract {

    private SalaryContract() {
    }

    public static final class MoneyEntry implements BaseColumns {
        public static final String TABLE_NAME = "money";

        public final static String _ID = BaseColumns._ID;
        public final static String COLUMN_DATE = "date";
        public final static String COLUMN_SUMM = "summ";
        public final static String COLUMN_MONTH = "month";
        public final static String COLUMN_YEAR = "year";
        public final static String COLUMN_NOTE = "note";


        public static final int MONTH_JAN = 1;
        public static final int MONTH_FAB = 2;
        public static final int MONTH_MAR = 3;
        public static final int MONTH_APR = 4;
        public static final int MONTH_MAY = 5;
        public static final int MONTH_JUN = 6;
        public static final int MONTH_JUL = 7;
        public static final int MONTH_AUG = 8;
        public static final int MONTH_SEP = 9;
        public static final int MONTH_OCT = 10;
        public static final int MONTH_NOV = 11;
        public static final int MONTH_DEC = 12;

    }
}
