package com.denver.wolfcontrol;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.IOException;

public class ReportActivity extends AppCompatActivity {

    public String fileName;
    public String fileList = " Сохраненные карты: \n";
    public String mainTotalFrame = "";
    public int hours1sum = 0;
    public int hours2sum = 0;
    public int hours3sum = 0;
    public int hours4sum = 0;
    public int money1sum = 0;
    public int money2sum = 0;
    public int money3sum = 0;
    public int money4sum = 0;
    public int totalSum = 0;
    public int fileCount = 0;
    public int mainCount = 0;
    public int submainCount = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        int day;
        Intent intent = getIntent();
        int month = intent.getIntExtra(StatActivity.REPORT_MONTH,0);
        int year = intent.getIntExtra(StatActivity.REPORT_YEAR,0);



        for(day = 1; day <= 31; day++) { // Walk through the day from 1 to 31

            // Make fileName
            String dayFileName;
            if (day-10 < 0) {
                dayFileName = "0" + day;
            } else dayFileName = String.valueOf(day);
            String monthFileName;
            if (month-10 < 0) {
                monthFileName = "0" + month;
            } else monthFileName = String.valueOf(month);
            String yearFileName = String.valueOf(year);

            fileName = dayFileName + monthFileName + yearFileName + ".txt";

            //Try to read DBFile fileName.txt (*date*.txt)
            FileInputStream fin = null;
            try {
                fin = openFileInput(fileName);
                byte[] bytes = new byte[fin.available()];
                fin.read(bytes);
                String buffer = new String(bytes);
                fin.close();

                // Processing file information
                char[] chArray = buffer.toCharArray();
                int k=0;

                String hours1 = "";
                for (int i = 0; i<chArray.length; i++){
                    k = i;
                    if (chArray[i] != '\n') {
                        hours1 = hours1 + chArray[i];
                    } else break; }
                if (hours1.equals("")) {} else
                hours1sum = hours1sum + Integer.parseInt(hours1);

                String hours2 = "";
                for (int i = k+1; i<chArray.length; i++){
                    k = i;
                    if (chArray[i] != '\n') {
                        hours2 = hours2 + chArray[i];
                    } else break; }
                if (hours2.equals("")) {} else
                hours2sum = hours2sum + Integer.parseInt(hours2);

                String hours3 = "";
                for (int i = k+1; i<chArray.length; i++){
                    k = i;
                    if (chArray[i] != '\n') {
                        hours3 = hours3 + chArray[i];
                    } else break; }
                if (hours3.equals("")) {} else
                hours3sum = hours3sum + Integer.parseInt(hours3);

                String hours4 = "";
                for (int i = k+1; i<chArray.length; i++){
                    k = i;
                    if (chArray[i] != '\n') {
                        hours4 = hours4 + chArray[i];
                    } else break; }
                if (hours4.equals("")) {} else
                hours4sum = hours4sum + Integer.parseInt(hours4);

                String money1 = "";
                for (int i = k+1; i<chArray.length; i++){
                    k = i;
                    if (chArray[i] != '\n') {
                        money1 = money1 + chArray[i];
                    } else break; }
                money1sum = money1sum + Integer.parseInt(money1);

                String money2 = "";
                for (int i = k+1; i<chArray.length; i++){
                    k = i;
                    if (chArray[i] != '\n') {
                        money2 = money2 + chArray[i];
                    } else break; }
                money2sum = money2sum + Integer.parseInt(money2);

                String money3 = "";
                for (int i = k+1; i<chArray.length; i++){
                    k = i;
                    if (chArray[i] != '\n') {
                        money3 = money3 + chArray[i];
                    } else break; }
                money3sum = money3sum + Integer.parseInt(money3);

                String money4 = "";
                for (int i = k+1; i<chArray.length; i++){
                    k = i;
                    if (chArray[i] != '\n') {
                        money4 = money4 + chArray[i];
                    } else break; }
                money4sum = money4sum + Integer.parseInt(money4);

                String summ = "";
                for (int i = k+1; i<chArray.length; i++){
                    k = i;
                    if (chArray[i] != '\n') {
                        summ = summ + chArray[i];
                    } else break;  }
                totalSum = totalSum + Integer.parseInt(summ);

                char swStatus = '0';
                String swStatusS;
                for (int i = k+1; i<chArray.length; i++){
                    k = i;
                    if (chArray[i] != '\n') {
                        swStatus = chArray[i];
                    } else break; }
                if (swStatus == '1') { swStatusS = "смена           ";
                                       mainCount++;
                                     } else {swStatusS = "подработка";
                                             submainCount++; }



                fileList = fileList + " " + dayFileName + "/" + monthFileName + "/" + yearFileName + "  " + swStatusS + " сумма: " + summ + "\n";



                } catch (IOException ex) {
                fileCount++;
            }
        }
        if (fileCount == 31) {
            Toast.makeText(this, "Нет информации за этот период!", Toast.LENGTH_LONG).show();
            finish();
        }

        // Rename number of the month in word
        String monthString;
        switch (month) {
            case 1:  monthString = "Январь";
                break;
            case 2:  monthString = "Февраль";
                break;
            case 3:  monthString = "Март";
                break;
            case 4:  monthString = "Апрель";
                break;
            case 5:  monthString = "Май";
                break;
            case 6:  monthString = "Июнь";
                break;
            case 7:  monthString = "Июль";
                break;
            case 8:  monthString = "Август";
                break;
            case 9:  monthString = "Сентябрь";
                break;
            case 10: monthString = "Октябрь";
                break;
            case 11: monthString = "Ноябрь";
                break;
            case 12: monthString = "Декабрь";
                break;
            default: monthString = "Не знаем такого";
                break;
        }

        TextView reportHeader = findViewById(R.id.reportHeader);
        reportHeader.setText(getString(R.string.reportHeader1) + " " + monthString + " " + year + " " + getString(R.string.reportHeader2));

        mainTotalFrame = " Общая сумма:"  + " " + totalSum + " грн." + "\n \n" +
                          " Количество смен: " + mainCount + "\n" +
                          " Количество подработок: " + submainCount + "\n \n" +
                           " Переплёт     : " + hours1sum + " часов \n" +
                           " Цифра          : " + hours2sum + " часов \n" +
                           " Комплектация : " + hours3sum + " часов \n" +
                           " Перебор брака: " + hours4sum + " часов \n \n" +
                            fileList;


        TextView reportBody = findViewById(R.id.reportBody);
        reportBody.setText(mainTotalFrame);


    }
}