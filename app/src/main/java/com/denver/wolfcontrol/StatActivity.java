package com.denver.wolfcontrol;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Calendar;

public class StatActivity extends AppCompatActivity {

    public final static String REPORT_MONTH = "REPORT_MONTH";
    public final static String REPORT_YEAR = "REPORT_YEAR";

    public int yearSet;
    public int monthSet = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stat);

        // get current year from Calendar
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        yearSet = year;

        // set current year in yearTextNumber
        EditText yearTextNumber = findViewById(R.id.yearTextNumber);
        String yearS = String.valueOf(year);
        yearTextNumber.setText(yearS);

        // Choosing Month and filling setMonthTextView (BEGIN of the module)
        Button janBtn = findViewById((R.id.janButton));
        janBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                TextView setMonthTextView = findViewById(R.id.setMonthTextView);
                setMonthTextView.setText("Январь");
                monthSet = 1;
            }
        });
        Button febBtn = findViewById((R.id.febButton));
        febBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                TextView setMonthTextView = findViewById(R.id.setMonthTextView);
                setMonthTextView.setText("Февраль");
                monthSet = 2;
            }
        });
        Button marBtn = findViewById((R.id.marButton));
        marBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                TextView setMonthTextView = findViewById(R.id.setMonthTextView);
                setMonthTextView.setText("Март");
                monthSet = 3;
            }
        });
        Button aprBtn = findViewById((R.id.aprButton));
        aprBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                TextView setMonthTextView = findViewById(R.id.setMonthTextView);
                setMonthTextView.setText("Апрель");
                monthSet = 4;
            }
        });
        Button mayBtn = findViewById((R.id.mayButton));
        mayBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                TextView setMonthTextView = findViewById(R.id.setMonthTextView);
                setMonthTextView.setText("Май");
                monthSet = 5;
            }
        });
        Button junBtn = findViewById((R.id.junButton));
        junBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                TextView setMonthTextView = findViewById(R.id.setMonthTextView);
                setMonthTextView.setText("Июнь");
                monthSet = 6;
            }
        });
        Button julBtn = findViewById((R.id.julButton));
        julBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                TextView setMonthTextView = findViewById(R.id.setMonthTextView);
                setMonthTextView.setText("Июль");
                monthSet = 7;
            }
        });
        Button augBtn = findViewById((R.id.augButton));
        augBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                TextView setMonthTextView = findViewById(R.id.setMonthTextView);
                setMonthTextView.setText("Август");
                monthSet = 8;
            }
        });
        Button sepBtn = findViewById((R.id.sepButton));
        sepBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                TextView setMonthTextView = findViewById(R.id.setMonthTextView);
                setMonthTextView.setText("Сентябрь");
                monthSet = 9;
            }
        });
        Button octBtn = findViewById((R.id.octButton));
        octBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                TextView setMonthTextView = findViewById(R.id.setMonthTextView);
                setMonthTextView.setText("Октябрь");
                monthSet = 10;
            }
        });
        Button novBtn = findViewById((R.id.novButton));
        novBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                TextView setMonthTextView = findViewById(R.id.setMonthTextView);
                setMonthTextView.setText("Ноябрь");
                monthSet = 11;
            }
        });
        Button decBtn = findViewById((R.id.decButton));
        decBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                TextView setMonthTextView = findViewById(R.id.setMonthTextView);
                setMonthTextView.setText("Декабрь");
                monthSet = 12;
            }
        });
        // Choosing Month and filling setMonthTextView (END of the module)

    }

    // Processing yearPlusButton
    public void plusYearClick (View view) {
        EditText yearTextNumber = findViewById(R.id.yearTextNumber);
        String yearS = yearTextNumber.getText().toString();
        int year = Integer.parseInt(yearS);
        year++;
        yearSet = year;
        yearS = String.valueOf(year);
        yearTextNumber.setText(yearS);
    }
    // Processing yearMinusButton
    public void minusYearClick (View view) {
        EditText yearTextNumber = findViewById(R.id.yearTextNumber);
        String yearS = yearTextNumber.getText().toString();
        int year = Integer.parseInt(yearS);
        year--;
        yearSet = year;
        yearS = String.valueOf(year);
        yearTextNumber.setText(yearS);
    }

    // Processing click on reportButton
    public void makeReport (View view) {
        // Get yearTextNumber value, because user can set year manually
        EditText yearTextNumber = findViewById(R.id.yearTextNumber);
        String yearS = yearTextNumber.getText().toString();
        int year = Integer.parseInt(yearS);

        // Check if the month is specified
        if(monthSet == 0) {
            Toast.makeText(this, "Пожалуйста, укажите месяц!", Toast.LENGTH_LONG).show();
            return;
        }
        // Check if the year not over period 2000-2100
        if(year < 2000 || year > 2100) {
            Toast.makeText(this, "Неверно указан год!", Toast.LENGTH_LONG).show();
            return;
        }
        // start Report Activity
        Intent intent = new Intent(this, ReportActivity.class);
        intent.putExtra(REPORT_MONTH, monthSet);
        intent.putExtra(REPORT_YEAR, year);
        startActivity(intent);
    }
}